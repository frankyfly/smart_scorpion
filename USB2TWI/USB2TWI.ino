/*
    Name:       USB2TWI.ino
    Created:	15.08.2018 15:48:28
    Author:     LAPTOP-FRANK\Brüggerhoff

  message examples

  Writing
  >> w 30 24 60		Write to devide 30 register 24 the value 60
  << \r\n				returns empty new line

  >> r 30 24			Read from device 30 register 24
  << 60\r\n			returns value

*/


#include <Wire.h>

#include <Messenger.h>
Messenger message = Messenger();

int getRegister(int twiAddress, int regAddress) {
  Wire.beginTransmission(twiAddress);
  Wire.write(regAddress);
  Wire.endTransmission(true);
  Wire.requestFrom(twiAddress, 1);
  return Wire.read();
}

void setRegister(int twiAddress, int regAddress, int regValue) {
  Wire.beginTransmission(twiAddress);
  Wire.write(regAddress);
  Wire.write(regValue);
  Wire.endTransmission(true);
}



void Serial_message_completed()
{
	//Serial.print("echo: ");
	//Serial.println(message.peek());


  if (message.checkString("r"))
  {
    int twi_device = message.readInt();
    int device_reg = message.readInt();
    int reg_value = getRegister(twi_device, device_reg);
    Serial.println(reg_value);
  }

  else if (message.checkString("w"))
  {
    int twi_device = message.readInt();
    int device_reg = message.readInt();
    int reg_value = message.readInt();
    setRegister(twi_device, device_reg, reg_value);
    Serial.println();
  }

  else
  {
    Serial.flush();
    Serial.println("0");
  }
}

void Serial_message()
{
  while ( Serial.available() ) message.process(Serial.read( ) );
}

void setup()
{
  Serial.begin(115200);
  Serial.println("| USB-2-TWI Device |");
//  Serial.println("Wire.begin ...");
  Wire.begin();        // join i2c bus (address optional for master)
  
 // Serial.println("message.attach ...");
  message.attach(Serial_message_completed);
//  Serial.println("starting loop...");
}

void loop()
{
  Serial_message();
}
